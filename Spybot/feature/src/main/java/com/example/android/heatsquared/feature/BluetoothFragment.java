package com.example.android.heatsquared.feature;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;


public class BluetoothFragment extends Fragment {


    private BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    ArrayAdapter<String> arrayAdapterForScannedDevices;
    int moduleLocationInList = 0;

    ArrayList<BluetoothDevice> devices = new ArrayList<BluetoothDevice>();


    int REQUEST_ENABLE_BT = 1;


    public BluetoothFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_bluetooth, container, false);

//        Initialize Bluetooth
       initAdapter();

//        Search button
        Button searchButton = rootView.findViewById(R.id.button_search_devices);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchDevices(v);
            }
        });

//        int MY_PERMISSIONS_REQUEST = 200;
//        int permissions= ContextCompat.checkSelfPermission (getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);
//        ActivityCompat.requestPermissions(getActivity(),
//                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                MY_PERMISSIONS_REQUEST);
        checkLocationPermission();


        IntentFilter intentFilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        getActivity().registerReceiver(mReceiver, intentFilter);

        Toast.makeText(getActivity(), "TOASTY", Toast.LENGTH_SHORT).show();
        arrayAdapterForScannedDevices = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);
        ListView listView = rootView.findViewById(R.id.listview_scanned_devices);
        listView.setAdapter(arrayAdapterForScannedDevices);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long l){
                if(!isBound) return;
                Message msg = Message.obtain(null,BluetoothService.MSG_CONNECT_DEVICE,0,0);
                msg.getData().putParcelable(BluetoothService.BLUETOOTH_DEVICE, devices.get(position));
                Log.i("MACAddress", devices.get(position).getAddress());
                Toast.makeText(getActivity(), devices.get(position).getAddress(), Toast.LENGTH_SHORT).show();
                try {
                    bluetoothMessenger.send(msg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });

        return rootView;
    }


    // Create a BroadcastReceiver for ACTION_FOUND.
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent.
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address

                String entry = deviceName + "\n" + deviceHardwareAddress;
//                Log.i("BT", deviceName + "\n" + deviceHardwareAddress);
                arrayAdapterForScannedDevices.add(entry);
                moduleLocationInList = arrayAdapterForScannedDevices.getPosition(entry);
                devices.add(device);
            }
        }
    };

//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//
//
//
//    }
    /** Messenger for communicating with the service.  */
    Messenger bluetoothMessenger = null;

    /** Flag indicating whether we have called bind on the service.  */

    boolean isBound = false;

    /**
     * Class for interacting with the main interface of the service.
     */
    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the object we can use to
            // interact with the service.  We are communicating with the
            // service using a Messenger, so here we get a client-side
            // representation of that from the raw IBinder object.
            bluetoothMessenger = new Messenger(service);
            isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            bluetoothMessenger = null;
            isBound = false;
        }
    };

    public void searchDevices(View view) {
        Toast.makeText(getActivity(), "Searching", Toast.LENGTH_SHORT).show();
        mBluetoothAdapter.startDiscovery();
    }

    @Override
    public void onDestroy() {
        getActivity().unregisterReceiver(mReceiver);
        super.onDestroy();

    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().bindService(new Intent(getActivity(), BluetoothService.class), connection,
                Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        // Unbind from service
        if(isBound) {
            getActivity().unbindService(connection);
            isBound = false;
        }

    }

    private void initAdapter() {
        if (mBluetoothAdapter == null) {
            Toast.makeText(getActivity(), "This device doesn't support Bluetooth functionality.",
                    Toast.LENGTH_SHORT).show();
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetoothIntent  = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBluetoothIntent, REQUEST_ENABLE_BT);
            }
        }

    }

    protected void checkLocationPermission() {
        int REQUEST_COARSE_LOCATION = 200;

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_COARSE_LOCATION);
        }
    }



}
