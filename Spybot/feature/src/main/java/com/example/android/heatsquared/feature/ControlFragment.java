package com.example.android.heatsquared.feature;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import io.github.controlwear.virtual.joystick.android.JoystickView;

public class ControlFragment extends Fragment {

    public static final String TAG = ControlFragment.class.getSimpleName();

    private static int REQUEST_ENABLE_BT = 1;

    WebView myWebView;

    private View rootView;

    private TextView mTextViewPositionLeft;
    private TextView mTextViewSteerPercentageLeft;
    private TextView mTextViewSteerVerticalPercentageLeft;

    private TextView mTextViewMotorPercentageeRight;

    private TextView mTextViewSensorData;

    private Switch onSwitch;
    private Switch cameraSwitch;

    private static final String HeatSquaredMACAddress = "20:14:03:28:28:60";
    private static final String OtherMACAddress = "20:17:04:24:92:42";


    private BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    private BluetoothDevice HeatSquaredBluetoothDevice ;


    ArrayList<String> dataToSend = new ArrayList<String>();
    private double motor = 15;
    private double carServo = 15;
    private double cameraVerticalServo = 0;
    private double cameraHorizontalServo = 0;

    // Data Timer
    private Timer sendDataTimer;
    private SendTask sendTask;
    private long DATA_PERIODIC_TIMER = 100;

    // Connect Timer
    private Timer connectTImer;
    private WatcherTask connectTask;
    private long CONNECT_PERIODIC_TIMER = 10000;
    private Boolean connected = false;


    private final double STEER_CAR_MAX_LEFT_VAL = 19.5;
    private final double STEER_CAR_NEUTRAL = 15.0;
    private final double STEER_CAR_MAX_RIGHT_VAL = 10.5;

    private final double STEER_MAX_DEGREES = 90;

    private final double MAX_PERCENT = 100;
    private final double MIN_PERCENT = 0;

    private final double MIN_MOTOR_SPEED = 15.0;
    private final double MAX_MOTOR_SPEED = 19.5;
    private final double MOTOR_DEGREE_LOW = 270;
    private final double MOTOR_DEGREE_HIGH = 90;

    double currentCarSteerPercentage = 0;
    double currentCameraVerticalSteerPercentage = 0;
    double currentCameraHorizontalSteerPercentage = 0;
    String steerHorizontalPosition = "left";
    String steerVerticalPosition = "forward";

    String steerCameraHorizontalPosition = "left";
    String steerCameraVerticalPosition = "up";

    double currentMotorPercentage = 0;

    public ControlFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_control, container, false);

        mTextViewPositionLeft = rootView.findViewById(R.id.textView_position_left);
        mTextViewSteerPercentageLeft = rootView.findViewById(R.id.textView_steer_percentage_left);

        mTextViewSteerVerticalPercentageLeft = rootView.findViewById(R.id.textView2_vertical_steer_percentage);

        JoystickView joystickLeft = rootView.findViewById(R.id.joystickView_left);
        joystickLeft.setOnMoveListener(new JoystickView.OnMoveListener() {
            @Override
            public void onMove(int angle, int strength) {

                double steerPercentage = (double)(angle % STEER_MAX_DEGREES)/ STEER_MAX_DEGREES;

                if(cameraSwitch.isChecked())
                {
                    mTextViewPositionLeft.setText(steerCameraHorizontalPosition);

                    if (angle >= 0 && angle < 180) {
                        steerCameraVerticalPosition = "up";
                    } else {
                        steerCameraVerticalPosition = "down";
                    }

                    currentCameraVerticalSteerPercentage = strength;

                    if (angle == 0 && strength == 0) {
                        currentCameraVerticalSteerPercentage = MIN_PERCENT;
                        currentCameraHorizontalSteerPercentage = MIN_PERCENT;
                    }
                    else if(angle >= 90 && angle < 270) {
                        steerCameraHorizontalPosition = "left";
                        currentCameraHorizontalSteerPercentage = steerPercentage *100;

                        if (angle >= 180) {
                            currentCameraHorizontalSteerPercentage = (1 - steerPercentage) * 100;
                        }

                        if(angle >= 140 && angle <= 200) {
                            currentCameraVerticalSteerPercentage = 0;
                            currentCameraHorizontalSteerPercentage = strength;
                        }


                    } else {
                        steerCameraHorizontalPosition = "right";
                        currentCameraHorizontalSteerPercentage = steerPercentage *100;

                        if (angle < 270) {
                            currentCameraHorizontalSteerPercentage = (1 - steerPercentage) * 100;
                        }

                        if (angle <= 40 || angle >= 320) {
                            currentCameraVerticalSteerPercentage = 0;
                            currentCameraHorizontalSteerPercentage = strength;
                        }

                    }


                    mTextViewSteerVerticalPercentageLeft.setText(String.format(Locale.US, "%.2f",currentCameraVerticalSteerPercentage) + "%");
                    mTextViewSteerPercentageLeft.setText(String.format(Locale.US, "%.2f",currentCameraHorizontalSteerPercentage) + "%");
//                    Log.i(TAG, String.valueOf(currentCameraSteerPercentage));
                }
                else {
                    mTextViewPositionLeft.setText(steerHorizontalPosition);
                    if ((angle == 0 && strength == 0) || angle == 90 || angle == 270) {
//                    Log.i(TAG, "straight");
                        currentCarSteerPercentage = MIN_PERCENT;
                    } else if (angle > 90 && angle < 270) {
                        steerHorizontalPosition = "left";
                        steerVerticalPosition = "forward";
                        currentCarSteerPercentage = steerPercentage * 100;
                        if (angle >= 180) {
                            steerVerticalPosition = "backward";
                            currentCarSteerPercentage = MAX_PERCENT;
                        }
                    } else if (angle < 90 || angle > 270) {
                        steerHorizontalPosition = "right";
                        steerVerticalPosition = "forward";
                        currentCarSteerPercentage = (1 - steerPercentage) * MAX_PERCENT;
                        if (angle > 270) {
                            steerVerticalPosition = "backward";
                            currentCarSteerPercentage = MAX_PERCENT;
                        }
                    }
                    mTextViewSteerPercentageLeft.setText(String.format(Locale.US, "%.2f", currentCarSteerPercentage) + "%");
//                    Log.i(TAG, String.valueOf(currentCarSteerPercentage));
//                    Log.i(TAG, steerHorizontalPosition);
//                    Log.i(TAG, String.valueOf(angle) + "  " + String.valueOf(strength));
                }

                cameraVerticalServo = currentCameraVerticalSteerPercentage;
                cameraHorizontalServo = currentCameraHorizontalSteerPercentage;
                carServo = currentCarSteerPercentage;
            }
        });


        mTextViewMotorPercentageeRight = rootView.findViewById(R.id.textView_motor_percentage_right);

        JoystickView joystickRight = rootView.findViewById(R.id.joystickView_right);
        joystickRight.setOnMoveListener(new JoystickView.OnMoveListener() {
            @Override
            public void onMove(int angle, int strength) {

                if(angle == MOTOR_DEGREE_LOW) {
                    currentMotorPercentage = (MAX_PERCENT - strength)/2;
                }
                else if(angle == MOTOR_DEGREE_HIGH) {
                    currentMotorPercentage = (strength + MAX_PERCENT)/2;
                }

//                Log.i(TAG, String.valueOf(currentMotorPercentage));
                mTextViewMotorPercentageeRight.setText(currentMotorPercentage + "%");
                motor = currentMotorPercentage;
            }
        });

        myWebView = rootView.findViewById(R.id.webview);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
//        myWebView.loadUrl("http://www.google.com");
        myWebView.loadUrl("http://192.168.43.32");

        initAdapter();
//        HeatSquaredBluetoothDevice = mBluetoothAdapter.getRemoteDevice(OtherMACAddress);
        HeatSquaredBluetoothDevice = mBluetoothAdapter.getRemoteDevice(HeatSquaredMACAddress);
        IntentFilter fromServiceIntentFilter = new IntentFilter(BluetoothService.ACTION_FROM_SERVICE);
        fromServiceIntentFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        fromServiceIntentFilter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        getActivity().registerReceiver(serviceRequestsReceiver, fromServiceIntentFilter);


        onSwitch = rootView.findViewById(R.id.on_switch);
        onSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    startSendDataTimer();
                } else
                {
                    sendDataTimer.cancel();
                    // stop motor when unchecked
                    motor = 0; // percent
                    sendData();
                }
            }
        });
        cameraSwitch = rootView.findViewById(R.id.camera_switch);
        cameraSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.v("Switch State=", ""+isChecked);
            }
        });

        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    /** Messenger for communicating with the service.  */
    Messenger bluetoothMessenger;

    /** Flag indicating whether we have called bind on the service.  */
    Boolean isBound = false;

    /**
     * Class for interacting with the main interface of the service.
     */
    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the object we can use to
            // interact with the service.  We are communicating with the
            // service using a Messenger, so here we get a client-side
            // representation of that from the raw IBinder object.
            bluetoothMessenger = new Messenger(service);
            isBound = true;
            connectToBluetoothModule();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            bluetoothMessenger = null;
            isBound = false;
            Log.i(TAG, "here");
        }
    };

    /**
     * Connect to a predefined Bluetooth module. MAC address is parsed from the global.properties
     */
    private void connectToBluetoothModule() {
        // connect to the Bluetoooth module when connection to service is established
        Message connectToDeviceMsg = Message.obtain(null, BluetoothService.MSG_CONNECT_DEVICE, 0, 0);
        connectToDeviceMsg.getData().putParcelable(BluetoothService.BLUETOOTH_DEVICE, HeatSquaredBluetoothDevice);
        try {
            sendMessage(connectToDeviceMsg);
        } catch (Exception e) {
            Log.e(TAG, "Error connecting to the device: ", e);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().bindService(new Intent(getActivity(), BluetoothService.class), connection,
                Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        // Unbind from service
        if(isBound) {
            getActivity().unbindService(connection);
            isBound = false;
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(serviceRequestsReceiver);

        if(sendDataTimer != null) {
            sendDataTimer.cancel();
        }
        if(connectTImer != null) {
            connectTImer.cancel();
        }
    }

    private BroadcastReceiver serviceRequestsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
//            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            if(BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                Toast.makeText(getActivity(), "Connected", Toast.LENGTH_SHORT).show();
                Log.i(TAG, "Receiver connect");
                connected = true;
            }
            else if(BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action))
            {
                Toast.makeText(getActivity(), "Disconnected", Toast.LENGTH_SHORT).show();
//                connectToBluetoothModule();
                Log.i(TAG, "Receiver disconnect");
                connected = false;
                startConnectTimer();
            }

            if(intent.hasExtra(BluetoothService.EXTRA_IP_ADDRESS)) {
                String url = intent.getStringExtra(BluetoothService.EXTRA_IP_ADDRESS);
                displayURL(url);

                // Send ack that it received ip address
                Message data = Message.obtain(null,BluetoothService.MSG_ACK, 0,0);
                sendMessage(data);
            }

            if(intent.hasExtra(BluetoothService.EXTRA_SENSOR_DISTANCE)) {
                String data = intent.getStringExtra(BluetoothService.EXTRA_SENSOR_DISTANCE);
                setDistanceText(data);

            }


        }
    };

    private void displayURL(String url) {
        myWebView.loadUrl(url);
    }

    private void setDistanceText(String millimeter) {

        mTextViewSensorData = rootView.findViewById(R.id.textView_sensor_distance);
        mTextViewSensorData.setText("Distance: " + millimeter + " mm");
    }


    private void initAdapter() {
        if (mBluetoothAdapter == null) {
            Toast.makeText(getActivity(), "This device doesn't support Bluetooth functionality.",
                    Toast.LENGTH_SHORT).show();
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetoothIntent  = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBluetoothIntent, REQUEST_ENABLE_BT);
            }
        }

    }

    private void sendMessage(Message msg) {
        try {
            bluetoothMessenger.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void sendData() {
        if(!isBound) return;
        dataToSend.add(0, String.valueOf(motor));
        dataToSend.add(1,String.valueOf(carServo));
        dataToSend.add(2, String.valueOf(cameraVerticalServo));
        dataToSend.add(3, steerHorizontalPosition);
        dataToSend.add(4,steerVerticalPosition);
        dataToSend.add(5, steerCameraVerticalPosition);
        dataToSend.add(6,steerCameraHorizontalPosition);
        dataToSend.add(7,String.valueOf(cameraHorizontalServo));

        Message data = Message.obtain(null, BluetoothService.MSG_DATA,0, 0);
        data.getData().putStringArrayList(BluetoothService.BLUETOOTH_SUBCOMMAND_DATA, dataToSend);
        sendMessage(data);
    }

    private void startSendDataTimer() {
        sendDataTimer = new Timer();
        sendTask = new SendTask();
        sendDataTimer.schedule(sendTask,0, DATA_PERIODIC_TIMER);

    }

    private void startConnectTimer() {
        connectTImer = new Timer();
        connectTask = new WatcherTask();
        connectTImer.schedule(connectTask, 0, CONNECT_PERIODIC_TIMER);
    }

    private class SendTask extends TimerTask {
        @Override
        public void run() {
                sendData();
        }
    }

    private class WatcherTask extends TimerTask {
        @Override
        public void run() {
            if(!connected && isBound && (bluetoothMessenger != null)) {
                Log.i(TAG, "Disconnected");
                connectToBluetoothModule();
//                HeatSquaredBluetoothDevice = mBluetoothAdapter.getRemoteDevice(HeatSquaredMACAddress);
            }
            else{
                Log.i(TAG, "cancelling");
                connectTImer.cancel();
            }
        }
    }


}
