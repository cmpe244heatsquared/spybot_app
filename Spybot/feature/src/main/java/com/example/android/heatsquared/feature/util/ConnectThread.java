package com.example.android.heatsquared.feature.util;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.example.android.heatsquared.feature.BluetoothService;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.io.BufferedReader;

public class ConnectThread extends Thread {

    public static final String TAG = ConnectThread.class.getSimpleName();

    public  ConnectedThread connectedThread = null;

    private BluetoothDevice bluetoothDevice;
    private Handler handler;

    private BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    private BluetoothSocket bluetoothSocket = null;

    private UUID UUID = java.util.UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    public ConnectThread(BluetoothDevice device, Handler handler) {
        bluetoothDevice = device;
        this.handler = handler;
        BluetoothSocket tmp = null;
        try {
            // Get a BluetoothSocket to connect with the given BluetoothDevice.
            // MY_UUID is the app's UUID string, also used in the server code.
            tmp = device.createRfcommSocketToServiceRecord(UUID);
        } catch ( IOException e) {
            Log.e(TAG, "Socket's create() method failed", e);
        }
        bluetoothSocket = tmp;
        }

    @Override
    public void run() {
        // Cancel discovery because it otherwise slows down the connection.
        bluetoothAdapter.cancelDiscovery();
        try {
            // Connect to the remote device through the socket. This call blocks
            // until it succeeds or throws an exception.
            bluetoothSocket.connect();

        } catch (IOException connectException) {
            // Unable to connect; close the socket and return.
            Log.i(TAG, "sad :(");
            try {
                bluetoothSocket.close();
            } catch (IOException closeException) {
                Log.e(TAG, "Could not close the client socket", closeException);
            }
            return;
        }

        // The connection attempt succeeded. Perform work associated with
        // the connection in a separate thread.
        //manageMyConnectedSocket(bluetoothSocket)
        connectedThread = new ConnectedThread(bluetoothSocket, handler);
        connectedThread.start();

    }

    // Closes the client socket and causes the thread to finish.
    public void cancel() {
        try {
            if(connectedThread != null) {
                connectedThread.cancel();
                bluetoothSocket.close();
            }
        } catch (IOException e) {
            String errorMessage = "Could not close the client socket";
            Log.e(TAG, errorMessage, e);
            Message msg = handler.obtainMessage(BluetoothService.MSG_ERROR, 0, 0);
            msg.getData().putString(BluetoothService.ERROR_MESSAGE, errorMessage + " " + e.getMessage());
            msg.sendToTarget();
        }
    }

    public void write(byte[] bytes) {
        try {
            connectedThread.write(bytes);
        } catch (Exception e) {
            String errorMessage = "Error writing to the connectedThread.";
            Log.e(TAG, errorMessage, e);
        }
    }


    private class ConnectedThread extends Thread {

        private InputStream inputStream;
        private OutputStream outputStream;

        private BluetoothSocket mSocket;
        private Handler handler;

        private byte[] buffer;


        public ConnectedThread(BluetoothSocket socket, Handler handler) {
            InputStream  tmpIn = null;
            OutputStream tmpOut = null;
//            Log.i(TAG, "connected constructor");
            this.handler = handler;
            mSocket = socket;

            // Get the input and output streams; using temp objects because
            // member streams are final.
            try {
                tmpIn = mSocket.getInputStream();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when creating input stream", e);
            }

            try {
                tmpOut = mSocket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when creating output stream", e);
            }

            inputStream = tmpIn;
            outputStream = tmpOut;
        }

        @Override
        public void run() {
            buffer = new byte[1024];
//            Log.i(TAG, "connected here");
            while (true) {
                try {
//                    BufferedReader response = new BufferedReader(StandardCharsets.US_ASCII).readLine();
                    BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                    String response = r.readLine();
                    Message msg = this.handler.obtainMessage(
                            BluetoothService.MSG_RECEIVE, 0, 0);
                    msg.getData().putString(BluetoothService.RECEIVE, response);
                    msg.sendToTarget();
                } catch (IOException e) {
                    Log.d(TAG, "Input stream was disconnected", e);
                    break;
                }

            }
        }

        // Call this from the main activity to send data to the remote device.
        public void write(byte[] bytes) {
            try {
                outputStream.write(bytes);

                String sentMessage = bytes.toString();
//                Log.i(TAG, "SENT MESSAGE: " + sentMessage);
                Message msg = this.handler.obtainMessage(BluetoothService.MSG_SENT, 0, 0);
                msg.getData().putString(BluetoothService.SENT_MESSAGE, sentMessage);
                msg.sendToTarget();
            } catch (IOException e) {
                String errorMessage = "Error occured when sending data";
                Log.e(TAG, errorMessage, e);
                Message msg = this.handler.obtainMessage(BluetoothService.MSG_ERROR, 0, 0);
                msg.getData().putString(BluetoothService.ERROR_MESSAGE, errorMessage);
                msg.sendToTarget();
            }
        }

        public void cancel() {
            try {
                mSocket.close();
            }catch (IOException e) {
                Log.e(TAG, "Could not close the connect socket", e);
            }
        }
    }
}
