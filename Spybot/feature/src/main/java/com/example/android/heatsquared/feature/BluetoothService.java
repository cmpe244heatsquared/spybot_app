package com.example.android.heatsquared.feature;

import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import android.widget.Toast;
import android.os.*;

import com.example.android.heatsquared.feature.util.ConnectThread;

import java.util.ArrayList;
import java.util.List;

public class BluetoothService extends Service {

    public static final String TAG = BluetoothService.class.getSimpleName();


    public static final Integer MSG_ERROR = 0;
    public static final Integer MSG_CONNECT_DEVICE = 1;
    public static final Integer MSG_DATA = 2;
    public static final Integer MSG_ACK = 3;
    public static final Integer MSG_RECEIVE = 6;
    public static final Integer MSG_SENT = 7;


    public static final String BLUETOOTH_DEVICE = "bluetoothDevice";
    public static final String BLUETOOTH_COMMAND = "bluetooth";

    public static final String ACTION_FROM_SERVICE = "ACTION_FROM_SERVICE";

    public static final String RECEIVE = "RECEIVE";
    public static final String RECEIVE_ADRRESS_TYPE = "address";
    public static final String IP_ADDRESS = "ip";
    public static final String ACK_MSG = "ack";


    public static final String RECEIVE_SENSOR_TYPE = "sensor";
    public static final String SENSOR_DISTANCE = "distance";

    public static final String DummyMsg = "D";

    public static final String EXTRA_IP_ADDRESS = "EXTRA_IP_ADDRESS";
    public static final String EXTRA_SENSOR_DISTANCE = "EXTRA_SENSOR_DISTANCE";


    public static final String BLUETOOTH_SUBCOMMAND_DATA = "data";

    // error message key
    public static final String  ERROR_MESSAGE = "ERROR_MESSAGE";

    // sent message key
    public static final String SENT_MESSAGE = "SENT_MESSAGE";

    private ConnectThread connectThread = null;

    private BluetoothDevice bluetoothDevice = null;

    public BluetoothService() {
    }


    Messenger messenger = new Messenger(new IncomingHandler());
    @Override
    public IBinder onBind(Intent intent) {
        return messenger.getBinder();
    }

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {

            if(msg.what == MSG_CONNECT_DEVICE) {
                handleConnectDevice(msg);
            }
            else if (msg.what == MSG_DATA) {
                handleDataMessage(msg);
            }
            else if (msg.what == MSG_ACK) {
                handleAckMessage();
            }
            else if(msg.what == MSG_RECEIVE) {
                handleMessageReceive(msg);
            }
            else if(msg.what == MSG_ERROR) {
                handleMessageError(msg);
            }
            else {
                super.handleMessage(msg);
            }
        }
    }

    private void handleConnectDevice(Message msg) {
        Toast.makeText(getApplicationContext(),"Connecting to device.",
                Toast.LENGTH_SHORT).show();
        bluetoothDevice = msg.getData().getParcelable(BLUETOOTH_DEVICE);
        connectDevice();
    }

    private void handleDataMessage(Message msg) {
        ArrayList<String> dataToSend = msg.getData().getStringArrayList(BLUETOOTH_SUBCOMMAND_DATA);
        String motorValue = dataToSend.get(0);
        String carServoValue = dataToSend.get(1);
        String cameraServoValue = dataToSend.get(2);
        String steerHorizontalPosition = dataToSend.get(3);
        String steerVerticalPosition = dataToSend.get(4);
        String steerCameraVeritcalPosition = dataToSend.get(5);
        String steerCameraHorizontalPosition = dataToSend.get(6);
        String steerCameraHorizontalValue = dataToSend.get(7);

        String command = BLUETOOTH_COMMAND + " " +
                BLUETOOTH_SUBCOMMAND_DATA + " " +
                motorValue + " " +
                carServoValue + " " +
                cameraServoValue + " " +
                steerHorizontalPosition + " " +
                steerVerticalPosition + " " +
                steerCameraVeritcalPosition + " " +
                steerCameraHorizontalPosition + " " +
                steerCameraHorizontalValue + " " + "\r\n";
        sendDataToController(command);

        Log.i(TAG, command);

    }

    private void handleAckMessage() {
        String command = BLUETOOTH_COMMAND + " " +
                ACK_MSG + " " +
                DummyMsg + " " +
                DummyMsg + " " +
                DummyMsg + " " +
                DummyMsg + " " + "\r\n";
        sendDataToController(command);

        Log.i(TAG, command);
    }

    private void handleMessageReceive(Message msg) {
        String receivedMessage = msg.getData().getString(RECEIVE);
        Log.i(TAG, "Message received: " + receivedMessage);
        if(receivedMessage.equals("LPC: ")) {
            Log.i(TAG, "fuck you");
        }
        else if(!receivedMessage.equals("")) {
            String[] splitMessage = receivedMessage.split("\\s");
            if (receivedMessage != null) {
                try {
                    if (splitMessage[0].equals(RECEIVE_ADRRESS_TYPE) || splitMessage[1].equals(RECEIVE_ADRRESS_TYPE)) {
                        handleAddressMessageType(splitMessage);
                    }
                    if (splitMessage[0].equals(RECEIVE_SENSOR_TYPE) || splitMessage[1].equals(RECEIVE_SENSOR_TYPE)) {
                        handleSensorMessageType(splitMessage);
                    }
                } catch(Exception e){
                    // I don't care bro
                }


            }
        }
    }

    private void handleAddressMessageType(String[] msg) {
        Intent intent = new Intent(ACTION_FROM_SERVICE);
        // " address ip 'url' or LPC: address ip 'url' "
        if(msg[1].equals(IP_ADDRESS)) {
            String value = msg[2];
            intent.putExtra(EXTRA_IP_ADDRESS, value);
            sendBroadcast(intent);
        }
        else if (msg[2].equals(IP_ADDRESS)) {
            String value = msg[3];
            intent.putExtra(EXTRA_IP_ADDRESS, value);
            sendBroadcast(intent);
        }

    }

    private void handleSensorMessageType(String[] msg) {
        Intent intent = new Intent(ACTION_FROM_SERVICE);
        // " sensor distance 'mm' or LPC: sensor distance 'mm' "
        if(msg[1].equals(SENSOR_DISTANCE)) {
            String value = msg[2];
            intent.putExtra(EXTRA_SENSOR_DISTANCE, value);
            sendBroadcast(intent);
        }
        else if (msg[2].equals(SENSOR_DISTANCE)) {
            String value = msg[3];
            intent.putExtra(EXTRA_SENSOR_DISTANCE, value);
            sendBroadcast(intent);
        }

    }


    private void handleMessageError(Message msg) {
        String receivedMessage = msg.getData().getString(ERROR_MESSAGE);
        Toast.makeText(getApplicationContext(), "Error: " + receivedMessage,
                Toast.LENGTH_SHORT).show();
    }

    private void connectDevice() {
        if (connectThread != null) {
            connectThread.cancel();
            connectThread = null;
        }
        connectThread = new ConnectThread(bluetoothDevice, new IncomingHandler());
        connectThread.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            connectThread.cancel();
        } catch (NullPointerException  e) {
            Log.e(TAG, "ConnectThread has not been initialized yet.", e);
        }
    }

    /**
     * Sends the data to the connected bridge
     */
    private void sendDataToController(String data) {
        connectThread.write(data.getBytes());
    }
}


